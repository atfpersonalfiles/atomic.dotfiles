#!/usr/bin/env bash

dir="$HOME/atomic.dotfiles/configurations/rofi/yorha/launcher"
theme='launcher'

## Run
rofi \
    -show drun \
    -terminal alacritty \
    -theme ${dir}/${theme}.rasi
