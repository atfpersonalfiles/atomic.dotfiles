# Contributing
## Issues
Issues created by you will be a way to note problems that others may not see.
Preferably, make issues if:
- They show where this project is lacking.
- They show a better way to write code or implement functionality.
- They add awareness and improve the user experience/understanding of the project.
- They highlight a problem or a breaking change.

Thank you for issuing.

## Pull Requests
Pull requests are ways to materialize new ideas into the project.

When deciding if a pull request will be pulled, it needs to meet the following criteria:

### State Clear Intent
It should be clear which problem (feature or fix) you're trying to add/solve with your contribution.

>Add link to code of conduct in README.md

Doesn't communicate which pain you're relieving. 

>Add link to code of conduct in README.md because users don't always look in the CONTRIBUTING.md

Tells me the problem that you have found, and the pull request shows me the action you have taken to solve it.

### Contain Good Quality
- There are clear examples of why or what the code you're adding is doing.
- Highly appreaciated to have images showing changes when pertaining.
- The spelling is clear and correct. Use LanguageTool to revise and write in **English** if you struggle with it, so discussions are more inclusive.

## ‼ ️Very Important Notice
Although this repository has a license, great README (biased), contributing guidelines, issues and merge templates - so it appears to be a very contributeable project - it does not mean I will be accepting every pull request or even have issues investigated for a couple of reasons:

- Features in the [atomic.dotfiles](https://gitlab.com/atfpersonalfiles/atomic.dotfiles) will be specific and oriented to my system and preferred workflow. I believe in sharing, and that said action is great for improvement through collaboration or competition, but it must be acknowledged that my configurations may not be for everyone. There are chances some choices are less applicable to you; it is better to create a fork with your needs and changes instead, if deemed necessary.

- Not every code may be correctly audited by me, meaning security compromises or code not very well understood by me in general, making it difficult to improve/change it later on, causing long-term inconveniences. (Skill issue)
