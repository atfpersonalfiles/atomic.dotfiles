# 💠 atomic.dotfiles

💠 Dotfiles for my linux system. They're a way to recover your configurations and customizations in files using a repo with program configurations and scripts. Uses symlinking to sync changes with the repo.

<hr>

- [💠 atomic.dotfiles](#-atomicdotfiles)
- [🖥️ `System Info, X11 And Wayland Setup`](#️-system-info-x11-and-wayland-setup)
- [🗄️ `Installing All Dotfiles, How To Install All Dependencies And Configurations`](#️-installing-all-dotfiles-how-to-install-all-dependencies-and-configurations)
  - [🪟 `Window Managers Directory, Hyprland, Qtile And Openbox`](#-window-managers-directory-hyprland-qtile-and-openbox)
    - [`Hyprland`](#hyprland)
    - [`Qtile`](#qtile)
    - [`Openbox`](#openbox)
  - [📝 `Dotfiles Installation Explained`](#-dotfiles-installation-explained)
    - [⚙️ `Configurations Directory`](#️-configurations-directory)
      - [🔗 `Symbolical Linking`](#-symbolical-linking)
    - [➕ `Extras Directory`](#-extras-directory)
    - [💾 `Post Installation Directory`](#-post-installation-directory)
  - [📜 `Starting Script`](#-starting-script)
- [📖 `Wiki`](#-wiki)
- [🪪 `License`](#-license)
- [➕ `Contributing`](#-contributing)

<br>

![Cover](https://i.imgur.com/nPTtiEB.png)

<div>
  <img src="https://i.imgur.com/EkqVNi2.png" alt="Notion-BetterShot" width="49%" />
  <img src="https://i.imgur.com/XS0d9KU.png" alt="Yorha-BetterShot" width="49%" />
</div>

<br>

# 🖥️ `System Info, X11 And Wayland Setup`

| Specification      | Reference                                                                                                                   |
| ------------------ | --------------------------------------------------------------------------------------------------------------------------- |
| OS's               | [EndeavourOS](https://endeavouros.com/)                                                                                     |
| WM's               | [Hyprland](https://hyprland.org/), [Qtile](http://www.qtile.org/), [Openbox](https://wiki.archlinux.org/title/openbox/)     |
| Compositors        | [Wayland](https://wiki.archlinux.org/title/wayland), [Picom](https://github.com/yshui/picom)                                |
| App Launcher       | [Rofi](https://github.com/davatorium/rofi/)                                                                                 |
| Bars               | [Waybar](https://github.com/Alexays/Waybar), [Tint2](https://gitlab.com/o9000/tint2), [EWW](https://github.com/elkowar/eww) |
| Systemfetcher      | [FastFetch](https://github.com/fastfetch-cli/fastfetch)                                                                     |
| Screen Lockers     | [Hyrplock](https://github.com/hyprwm/hyprlock), [BetterScreenLocker](https://github.com/betterlockscreen/betterlockscreen)  |
| Wallpaper Handlers | [SWWW](https://github.com/Horus645/swww), [Feh](https://feh.finalrewind.org/)                                               |
| Terminal           | [Kitty](https://github.com/kovidgoyal/kitty)                                                                                |
| Shell              | [ZSH](https://www.zsh.org/), [ZInit](https://github.com/zdharma-continuum/zinit)                                            |
| Text Editors       | [Emacs](https://www.gnu.org/software/emacs/), [VSCodium](https://vscodium.com/)                                             |
| Web Browser        | [Brave](https://brave.com/), [Firefox](https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com)              |
| Music Players      | [NCMPCPP](https://github.com/ncmpcpp/ncmpcpp), [Spotify](https://www.spotify.com/)                                          |
| File Managers      | [Dolphin](https://apps.kde.org/dolphin/), [Ranger](https://github.com/ranger/ranger)                                        |

# 🗄️ `Installing All Dotfiles, How To Install All Dependencies And Configurations`

All dependencies necessary to grant correct functioning of all configurations are inside the `startingScript` so just follow the instructions below.
Type The Following Snippets Into Your Terminal

_To paste information in your terminal use ctrl + shift + v or shift + insert_

Clone The Repo To Your Home Folder

```
git clone https://gitlab.com/atfpersonalfiles/atomic.dotfiles.git
```

Goes Into The Cloned Repository

```
cd atomic.dotfiles
```

Turn The Script Into A Executable And Execute It!

```
sudo chmod +x startingScript
```

```
./startingScript
```

Now, Just Follow All The Instructions Inside The Script Itself.

## 🪟 `Window Managers Directory, Hyprland, Qtile And Openbox`

### `Hyprland`

A dynamic window manager and a wayland compositor that have great animations and greater customizability, uses a newer method of rendering windows and effects.

### `Qtile`

Another dynamic window manager with a ton of cool features written in python and super easy to fiddle with. Uses rofi as a launcher and powermenu and EWW bar to display workspaces and widgets. Old and reliable.

### `Openbox`

A floating window manager that's versatile and very well known.

## 📝 `Dotfiles Installation Explained`

The dotfiles contained within this repository are tailored for Arch-based systems. Dotfiles serve as a means to preserve the configurations of programs and system behavior in files and folders, offering optimal reproducibility in scenarios where system issues arise or when transferring configurations to another system is desired.

Allow me to elaborate on the repository's folder structure, should you wish to delve into it and experiment with some of my configurations. Additionally, I'll underscore the significance of the `startingScript`, which streamlines the process of installing and configuring everything within the system in a more automated fashion.

### ⚙️ `Configurations Directory`

The configurations directory contains the following:

- Backgrounds - Wallpapers for each theme;
- Bin - Important scripts to make the system work harmonically;
- Fonts - Typography used in various programs;
- FastFetch - A faster system fetcher;
- Dunst - Notification daemon;
- Eww - Main bar and widget handler in qtile and openbox (yorha theme);
- Tint2 - Main bar in Notion openbox theme;
- Picom - System compositor for X11;
- Rofi - App Menu And Powermenu;
- Waybar - A system bar for wayland compositors.
- Hyprlock - A screenlocker for wayland hyprland.

Scripts contained in the `bin` folder are as follow:

- `/bin/system`:
  - compositor, globalStartup, launchBar, notification, setWall and swwwChanger.
    > A script to kill and start picom and dunst with custom configurations and a globalStartup that aims to work in all WM's in the repository aswell as a bar launching script that opens different bars relative to the selected WM. To finish, we have a wallpaper slideshow applying script for xorg system and wayland respectively.
- `/bin/themes`:
  - Changer, ThemeFunctions, Yorha, Notion.
    > A changer script for both or more themes, fuctions boilerplates to be able to create more themes and a imperative population of functions for the respective themes that changes content inside other programs configurations using sed to change the system appearence quickly. Features a light and dark theme respectively.
- `/bin/utilities`:
  - colorGPick, defineQT5, keyboardConfig, monitorConfig, mouseConfig, notificationFix
    > colorpicker, defining QT5 theme in environment, hardware specific configurations, fix to prevent dunst from breaking after having plasma, avoiding conflicts with it's notification handling method.

Every configuration and script is centralized and makes it so it avoids conflict with already existing configurations in your system making updates in the structure much easier with the choice to not copy those configurations outside the repository itself.

Files and folders that need to be outside in order to work such as `fonts` use symbolical linking (Explained below) so any change can be too synced with the repository.

#### 🔗 `Symbolical Linking`

When you symbolically link folders from the repository to your system, any changes made to these linked folders within your system will reflect in the repository as well. This bidirectional linkage ensures that modifications made in either location are synchronized. Simply put, alterations made in the repository propagate to the linked folders in your system, and vice versa. To synchronize changes from the repository to your system, navigate to the repository directory (cd atomic.dotfiles) and execute git pull. You can verify any new changes using git status and fetch them with git pull.

It's advisable to rerun the startingScript after pulling changes from the repository, ensuring that everything continues to function seamlessly with your desired configurations.

If you've made alterations to files locally and wish to perform a git pull, you may encounter conflicts. In such cases, it's recommended to execute git reset --hard HEAD beforehand to ensure a clean pull and synchronization.

### ➕ `Extras Directory`

The extras directory hold optional software that ranges from the music player NCMPCPP to the text editor Emacs. They are different in nature from what the window-managers need and can be optionally used with them.

- `extras/emulators`:
  - Kitty - A GPU accelerated terminal emulator configurable and customizable.
- `extras/file-manager`:
  - ranger - A terminal-based file manager.
- `extras/music-player`
  - NCMPCPP - A highly configurable music player.
  - MPD - A music daemon that works with ncmpcpp.
- `extras/shell`:
  - .zshrc - Configuration file for the zsh shell.
- `extras/text-editor`:
  - Emacs - A extremely customizable IDE with my configurations.
  - VSCode - One the most popular IDEs with some custom configurations o' mine.

### 💾 `Post Installation Directory`

A script carefully made to install the programs that are use in a daily basis.
The script is separed in the following sections:

- Internet;
- Development;
- Video, Image And Audio;
- CLI Only;
- Utilities;
- Hardware;
- Security;
- Gaming;
- Terminal;

Every section offers you to install everything in them, just select some programs in it or skip a section entirely.

## 📜 `Starting Script`

The starting script acts similarly to a TUI (A very simple one at that) and uses while statements with functions, use the arrow keys to move down and upwards between available items and left and right to confirm or cancel all selections. To make selections, use your space bar.
I hope the script experience should be self-sufficient in conveying its messages and purposes. It is made to function without requiring further explanation. However, I appreciate any suggestions for improvements if deemed necessary. If you encounter any problems or have misunderstandings, please feel free to create an issue describing your problem. Thank you!

# 📖 `Wiki`

If you want to know more about all WM's, Emacs keybindings, theme tokens, fonts, trivias and credits from resources used in this repo, visit the [wiki](https://gitlab.com/atfpersonalfiles/atomic.dotfiles/-/wikis/Home)!

**You can find many useful links in the System Information section at the start of this file.**

# 🪪 `License`

This repo uses the [GNU GENERAL PUBLIC LICENSE V3](LICENSE.md).
See [Choose A License](https://choosealicense.com/licenses/gpl-3.0/) to understand it's implications.

# ➕ `Contributing`

There is a [contributing](CONTRIBUTING.md) file in this repository. See it before making issues or pull requests.

[⬆️ Back To Top](#-atomicdotfiles)
