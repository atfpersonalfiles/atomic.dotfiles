
;;; Code:
(add-to-list 'custom-theme-load-path "~/.config/emacs/themes/")
(load-theme 'notion t)

(provide 'assigned-theme)
;;; assigned-theme.el ends here
