(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("ad8735f8d74d31d34ec275625c7bb5c5f63f58190cb0434a533e6cf21f596e21" "de71af179ef9a1a26a521cb742e933422e997584b57aa2d090fa64b2aa578cd0" "137a918bdd26ba39f6ead9eb55f0f4d7e78ac1249fcabaee3588705c0ff94f1d" "a2f27e229ab93879df4c2381c115dfd9890dc8e1d39524ae67f7de3ed36fd736" "676b266e744ccbf05882d67a62729e47192c46e3ef2e006325bd751a31fab1c7" "729685230c2d53e872e7cdac746e5e7d82dc7a4009e77fa324b1de441c320eeb" "2b3bfd036565393fb88804b321f49d37d614119699ea986e7dfbc89e82a3dff7" "88f7ee5594021c60a4a6a1c275614103de8c1435d6d08cc58882f920e0cec65e" "a13eba6a72ef9e8e7c8fbf2ff60f17aee86db3cdd827c77c74ce1f0346f821ca" "691d671429fa6c6d73098fc6ff05d4a14a323ea0a18787daeb93fde0e48ab18b" default))
 '(package-selected-packages '(evil spacemacs-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
