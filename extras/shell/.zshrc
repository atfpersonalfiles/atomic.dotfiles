# Export Environment Variables
  # Standard Pathing For Scripts And Local Software
    #export PATH=$HOME/bin:/usr/local/bin:$PATH
    
  # Export Terminal Variable For SSH Use
    #export TERM=xterm-kitty
    export EDITOR=nvim
    export VISUAL=nvim

  # pnpm Path
    export PNPM_HOME="$HOME/.local/share/pnpm"
    export PATH="$PNPM_HOME:$PATH"

  # Spicetify Path
    export PATH=$PATH:$HOME/.spicetify

  # NVM
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

  # Default Screenshot Path
    export XDG_SCREENSHOTS_DIR=$HOME/Pictures/Screenshots

  # Avoid Ranger Default Configuratin
    export RANGER_LOAD_DEFAULT_RC=false

  # Export Tern
    export PATH="$HOME/go/bin:$PATH"

  # Export TEMP
    export TMPDIR=~/tmp/

# Sets QT Theme
  defineQT5="$HOME/atomic.dotfiles/configurations/bin/utilities/defineQT5"
  if [[ -f defineQT5 ]]; then
    . ~/defineQT5
  fi
#

# Theming
SPACESHIP_USER_SHOW="always" # Shows System user name before directory name
SPACESHIP_PROMPT_ASYNC=false
SPACESHIP_TIME_SHOW=true

SPACESHIP_PROMPT_ADD_NEWLINE=false
SPACESHIP_PROMPT_SEPARATE_LINE=true # Make the prompt span across two lines
# SPACESHIP_DIR_TRUNC=1 # Shows only the last directory folder name
 
SPACESHIP_CHAR_SYMBOL="❯"
SPACESHIP_CHAR_SUFFIX=" "

LS_COLORS=$LS_COLORS:'ow=01;34:' ; export LS_COLORS
 
SPACESHIP_PROMPT_ORDER=(
  time
  user          # Username section
  dir           # Current directory section
  host          # Hostname section
  node          # Display node version
  git           # Git section (git_branch + git_status)
  hg            # Mercurial section (hg_branch  + hg_status)
  exec_time     # Execution time
  line_sep      # Line break
  jobs          # Background jobs indicator
  exit_code     # Exit code section
  char          # Prompt character
)

# Enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
COMPLETION_WAITING_DOTS="true"

HIST_STAMPS="dd/mm/yyyy"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# On-demand rehash
zshcache_time="$(date +%s%N)"

autoload -Uz add-zsh-hook

rehash_precmd() {
  if [[ -a /var/cache/zsh/pacman ]]; then
    local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
    if (( zshcache_time < paccache_time )); then
      rehash
      zshcache_time="$paccache_time"
    fi
  fi
}

# Utils
## Runs `FastFetch` In Home If You Open Your Terminal
ff() {
    fastfetch -c $HOME/atomic.dotfiles/configurations/fastfetch/config.jsonc
}

if [ $PWD = $HOME ];
  then
      ff
 fi

add-zsh-hook -Uz precmd rehash_precmd

# clear
alias cl="clear"

# neovim as vim
alias vim="nvim"

alias zshrc="nvim ~/.zshrc"

# ls
alias l='ls -lh'
alias ll='ls -lah'
alias la='ls -A'
alias lm='ls -m'
alias lr='ls -R'
alias lg='ls -l --group-directories-first'

# git
alias gcl='git clone --depth 1'
alias gi='git init'
alias ga='git add'
alias gc='git commit -m'
alias gp='git push'

# package managers
alias pn="pnpm" 
alias ya="yarn"

# yt-dlp
alias dlp-audio="yt-dlp -x --audio-format m4a"
alias dlp-video="yt-dlp -f mp4"

# loopback
alias lp="pw-loopback -l .1"

zstyle ':omz:update' mode reminder  # just remind me to update when it's time
### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit.

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk

### Custom Parameters --------------------------------
# ZInit Plugins
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions
zinit light zdharma/fast-syntax-highlighting
zinit light spaceship-prompt/spaceship-prompt
