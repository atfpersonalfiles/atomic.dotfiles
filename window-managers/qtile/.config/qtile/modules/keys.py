from libqtile.config import Key, KeyChord, Click, Drag
from libqtile.lazy import lazy

# Vars
## Special Keys
mod = "mod4"
alt = "mod1"

## Apps Variables
term = "kitty"
musicPlayer = term + " -e ncmpcpp"
fileManager = term + " ranger"
printScreenUtil = "flameshot"
browser = "firefox"
brightnessUp = "brillo -q -A 10"
brightnessDown = "brillo -q -U 10"
screenlocker = "betterlockscreen --lock"
textEditor = "emacsclient -c -a emacs"
## Scripts
launcher = "atomic.dotfiles/configurations/rofi/notion/launcher/launcher.sh"
powermenu = "atomic.dotfiles/configurations/rofi/notion/powermenu/powermenu.sh"
changer = "atomic.dotfiles/configurations/bin/themes/Changer"
colorPicker = "atomic.dotfiles/configurations/bin/utilities/colorGPick"


def init_mouse():
    return [
        Drag(
            [mod],
            "Button1",
            lazy.window.set_position_floating(),
            start=lazy.window.get_position(),
        ),
        Drag(
            [mod],
            "Button3",
            lazy.window.set_size_floating(),
            start=lazy.window.get_size(),
        ),
        Click([mod], "Button2", lazy.window.bring_to_front()),
    ]


def init_keys():
    return [
        # Windows Controls And Navigation Commands
        Key([mod, "shift"], "r", lazy.restart(), desc="Restart Qtile."),
        Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile."),
        Key([mod], "c", lazy.window.kill(), desc="Kills active window."),
        Key([mod], "Tab", lazy.next_layout(), desc="Toggles through layouts forwards."),
        Key(
            [mod, "shift"],
            "Tab",
            lazy.prev_layout(),
            desc="Toggles through layouts backwards.",
        ),
        Key(
            [mod],
            "j",
            lazy.layout.down(),
            desc="Moves focus down/left in the current stack pane/layout.",
        ),
        Key(
            [mod, "shift"],
            "j",
            lazy.layout.shuffle_down(),
            lazy.layout.section_down(),
            desc="Move windows down in current stack",
        ),
        Key(
            [mod],
            "k",
            lazy.layout.up(),
            desc="Moves focus up/right in the current stack pane/layout.",
        ),
        Key(
            [mod, "shift"],
            "k",
            lazy.layout.shuffle_up(),
            lazy.layout.section_up(),
            desc="Move windows up in current stack.",
        ),
        Key([mod], "n", lazy.layout.normalize(), desc="Normalize window size ratios."),
        Key(
            [mod, "shift"],
            "n",
            lazy.layout.maximize(),
            desc="Toggle window between minimum and maximum sizes.",
        ),
        Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen."),
        Key(
            [mod, "control"],
            "f",
            lazy.window.toggle_floating(),
            desc="Toggle floating.",
        ),
        ###
        # Specific Layouts Controls
        Key(
            [mod],
            "h",
            lazy.layout.shrink(),
            lazy.layout.decrease_nmaster(),
            desc="Shrink window. (MonadTall) Decrease number in master pane. (Tile)",
        ),
        Key(
            [mod],
            "l",
            lazy.layout.grow(),
            lazy.layout.increase_nmaster(),
            desc="Expand window. (MonadTall) Increase number in master pane. (Tile)",
        ),
        Key(
            [mod, "control"],
            "Tab",
            lazy.layout.rotate(),
            lazy.layout.flip(),
            desc="Switch which side main pane occupies. (XmonadTall)",
        ),
        Key(
            [mod, "shift"],
            "h",
            lazy.layout.move_left(),
            desc="Move up a section. (TreeTab)",
        ),
        Key(
            [mod, "shift"],
            "l",
            lazy.layout.move_right(),
            desc="Move down a section (TreeTab)",
        ),
        Key(
            [mod],
            "space",
            lazy.layout.next(),
            desc="Switch window focus to other pane(s). (Stack)",
        ),
        Key(
            [mod, "control"],
            "space",
            lazy.layout.client_to_next(),
            desc="Switch window side to other pane(s). (Stack).",
        ),
        Key(
            [mod, "shift"],
            "space",
            lazy.layout.toggle_split(),
            desc="Toggle between split and unsplit sides. (Stack)",
        ),
        ###
        # Applications Shortcuts
        Key([alt], "F1", lazy.spawn(launcher), desc="Application launcher (Rofi)"),
        Key([mod], "x", lazy.spawn(powermenu), desc="Powermenu (Rofi)"),
        Key([mod], "t", lazy.spawn(changer), desc="Theme changer (Rofi)"),
        Key([mod], "Return", lazy.spawn(term), desc="Terminal emulator."),
        Key([mod], "b", lazy.spawn(browser), desc="Browser."),
        Key([mod], "m", lazy.spawn(musicPlayer), desc="Music player."),
        Key(
            [mod, "shift"],
            "e",
            lazy.spawn(textEditor),
            desc="Text editor and IDE.",
        ),
        Key([mod, "shift"], "f", lazy.spawn(fileManager), desc="File manager."),
        # Multi Monitor Controls
        Key([mod], "q", lazy.to_screen(0), desc="Keyboard focus to monitor 1."),
        Key([mod], "w", lazy.to_screen(1), desc="Keyboard focus to monitor 2."),
        Key([mod], "e", lazy.to_screen(2), desc="Keyboard focus to monitor 3."),
        Key([mod], "period", lazy.next_screen(), desc="Move focus to next monitor."),
        Key([mod], "comma", lazy.prev_screen(), desc="Move focus to prev monitor."),
        ###
        # Miscellaneous
        Key([mod], "p", lazy.spawn(colorPicker), desc="Color picker. (GPick)"),
        Key(
            [],
            "Print",
            lazy.spawn(printScreenUtil + " full"),
            desc="Takes a screenshot.",
        ),
        Key(
            ["shift"],
            "Print",
            lazy.spawn(printScreenUtil + " gui"),
            desc="Takes a screenshot of a screen area.",
        ),
        Key(
            ["control", "mod1"],
            "l",
            lazy.spawn(screenlocker),
            desc="Locks the screen.",
        ),
        Key(
            [mod],
            "XF86MonBrightnessUp",
            lazy.spawn(brightnessUp),
            desc="Mathematically increases backlight brightness.",
        ),
        Key(
            [mod],
            "XF86MonBrightnessDown",
            lazy.spawn(brightnessDown),
            desc="Mathematically decreases backlight brightness.",
        ),
        Key(
            [mod],
            "z",
            lazy.hide_show_bar("bottom"),
            lazy.hide_show_bar("top"),
            desc="Hides Qtile default bar in the top and bottom.",
        ),
        ###
    ]
