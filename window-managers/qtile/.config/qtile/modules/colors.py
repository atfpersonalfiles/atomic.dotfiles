colors = {
    "background": "#202020",
    "foreground": "#2C2C2C",
    "accent": "#1A2735",
    "black": "#5A5A5A",
    "white": "#D4D4D4",
    "red": "#DF5452",
    "green": "#529E72",
    "blue": "#5E87C9",
}
