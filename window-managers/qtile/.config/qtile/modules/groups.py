from libqtile.config import Group, Match
import re

def init_groups():
    return [
        Group("DEV", layout='treetab'),
        Group("WWW", layout='zoomy', matches=[
            Match(wm_class=re.compile(r"^(firefox|brave\-browser)$"))]),
        Group("SYS", layout='monadtall'),
        Group("DOC", layout='monadtall'),
        Group("CHAT", layout='treetab'),
        Group("MED", layout='treetab'),
        Group("GFX", layout='treetab', matches=[
            Match(wm_class=re.compile(r"^(kdeconnect\-app)$"))]),
        Group("VBOX", layout='monadtall'),
    ]
