import os
from libqtile import bar, widget
from libqtile.config import Screen
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration
from modules.colors import colors


widget_defaults = dict(
    font="JetBrainsMono Nerd Font",
    fontsize=14,
)

extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        widget.Spacer(length=12),
        widget.WindowCount(
            padding=12,
            text_format="{num} window(s)",
            decorations=[
                RectDecoration(
                    colour=colors["foreground"],
                    radius=4,
                    filled=True,
                    padding_y=4,
                    padding_x=0,
                    group=True,
                    clip=True,
                )
            ],
        ),
        widget.Sep(
            padding=12,
            foreground=colors["background"],
        ),
        widget.CurrentLayoutIcon(
            foreground=colors["black"],
            padding=4,
            scale=0.6,
        ),
        widget.CurrentLayout(
            foreground=colors["black"], padding=5, **extension_defaults
        ),
        widget.WindowName(
            foreground=colors["black"], max_chars=40, **extension_defaults
        ),
        widget.Spacer(),
        widget.Clock(
            foreground=colors["black"],
            format="%d/%m/%y %H:%M",
            **extension_defaults,
        ),
        widget.Spacer(),
        widget.Image(
            filename="~/.config/qtile/images/Volume.png",
            scale="False",
            margin_y=8,
            padding=12,
            decorations=[
                RectDecoration(
                    colour=colors["accent"],
                    radius=4,
                    filled=True,
                    padding_x=0,
                    padding_y=4,
                    group=True,
                    clip=True,
                )
            ],
            **extension_defaults,
        ),
        widget.Volume(
            foreground=colors["white"],
            fmt="Vol: {}",
            padding=0,
            decorations=[
                RectDecoration(
                    colour=colors["accent"],
                    radius=4,
                    filled=True,
                    padding_y=4,
                    padding_x=0,
                    group=True,
                    clip=True,
                )
            ],
            **extension_defaults,
        ),
        widget.Sep(
            padding=12,
            foreground=colors["accent"],
            decorations=[
                RectDecoration(
                    colour=colors["accent"],
                    radius=4,
                    filled=True,
                    padding_x=0,
                    padding_y=4,
                    group=True,
                    clip=True,
                )
            ],
        ),
        widget.Spacer(length=8),
        widget.Image(
            filename="~/.config/qtile/images/Battery.png",
            scale="False",
            padding=12,
            margin_y=8,
            decorations=[
                RectDecoration(
                    colour=colors["accent"],
                    radius=4,
                    filled=True,
                    padding_x=0,
                    padding_y=4,
                    group=True,
                    clip=True,
                )
            ],
            **extension_defaults,
        ),
        widget.Battery(
            **extension_defaults,
            padding=0,
            show_short_text=True,
            decorations=[
                RectDecoration(
                    colour=colors["accent"],
                    radius=4,
                    filled=True,
                    padding_x=0,
                    padding_y=4,
                    group=True,
                    clip=True,
                )
            ],
        ),
        widget.Sep(
            padding=12,
            foreground=colors["accent"],
            decorations=[
                RectDecoration(
                    colour=colors["accent"],
                    radius=4,
                    filled=True,
                    padding_x=0,
                    padding_y=4,
                    group=True,
                    clip=True,
                )
            ],
        ),
        widget.Spacer(length=8),
        widget.Systray(**extension_defaults),
        widget.Spacer(length=12),
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    if not os.listdir("/sys/class/power_supply/"):
        del widgets_screen1[11:15]

    return widgets_screen1


# All other monitors' bars will display everything but widget 10 (systray).
def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[17]

    if not os.listdir("/sys/class/power_supply/"):
        del widgets_screen2[11:15]

    return widgets_screen2


# For adding transparency to your bar, add (background="#00000000") to the "Screen" line(s)
# For ex: Screen(top=bar.Bar(widgets=init_widgets_screen2(), background="#00000000", size=24)),


def init_screens():
    return [
        Screen(
            bottom=bar.Bar(
                widgets=init_widgets_screen1(), background=colors["background"], size=40
            )
        ),
        Screen(
            bottom=bar.Bar(
                widgets=init_widgets_screen2(), background=colors["background"], size=40
            )
        ),
        Screen(
            bottom=bar.Bar(
                widgets=init_widgets_screen2(), background=colors["background"], size=40
            )
        ),
    ]


if __name__ == "__main__":
    screens = init_screens()
    bar.config(screens)  # Apply the bar configuration
