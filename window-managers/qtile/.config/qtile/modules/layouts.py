from libqtile import layout
from modules.colors import colors

layout_theme = {
    "border_width": 3,
    "margin": 14,
    "border_focus": colors["foreground"],
    "border_normal": colors["white"],
}


def init_layouts():
    return [
        # layout.MonadWide(**layout_theme),
        # layout.Bsp(**layout_theme),
        # layout.Stack(stacks=2, **layout_theme),
        # layout.Columns(**layout_theme),
        # layout.RatioTile(**layout_theme),
        # layout.Tile(shift_windows=True, **layout_theme),
        # layout.VerticalTile(**layout_theme),
        # layout.Matrix(**layout_theme),
        layout.MonadTall(**layout_theme),
        layout.Max(**layout_theme),
        layout.TreeTab(
            font="JetBrainsMono-BoldItalic",
            fontsize=12,
            sections=["First", "Second", "Third"],
            section_fg=colors["red"],
            section_fontsize=16,
            section_padding=16,
            border_width=0,
            bg_color=colors["background"],
            active_bg=colors["accent"],
            active_fg=colors["white"],
            inactive_bg=colors["foreground"],
            inactive_fg=colors["white"],
            padding_y=6,
            padding_x=6,
            padding_left=10,
            section_left=10,
            section_top=20,
            section_bottom=20,
            margin_left=10,
            level_shift=20,
            vspace=16,
            panel_width=270,
        ),
        layout.Zoomy(
            **layout_theme,
            columnwidth=200,
        ),
        layout.Stack(
            border_focus=colors["white"],
            border_normal=colors["foreground"],
            border_width=2,
            margin=14,
            num_stacks=2,
        ),
        layout.RatioTile(
            **layout_theme,
        ),
    ]
