# -*- coding: utf-8 -*-
# Qtile Main
from libqtile import qtile

# Autostart
import os
import subprocess
from libqtile import hook

# Groups
from modules.groups import init_groups
from libqtile.dgroups import simple_key_binder

# Keys
from modules.keys import init_keys, init_mouse

# Layouts
from libqtile import layout
from modules.layouts import init_layouts
from modules.layouts import layout_theme
from libqtile.config import Match

# Bar
import modules.notionBar


# Autostart
@hook.subscribe.startup
def autostart():
    home = os.path.expanduser("~/.config/qtile/autostart")
    subprocess.call([home])


# Keybindings Mouse Configs
keys = init_keys()
mouse = init_mouse()
cursor_warp = False

# widgets_list = init_widgets_list()
# widgets_screen1 = init_widgets_screen1()
# widgets_screen2 = init_widgets_screen2()

screens = modules.notionBar.init_screens()


# Groups
dgroups_key_binder = simple_key_binder("mod4")
dgroups_app_rules = []
groups = init_groups()

layouts = init_layouts()
floating_layout = layout.Floating(
    **layout_theme,
    float_rules=[
        #     # https://github.com/qtile/qtile/issues/1237
        #     # Run the utility of `xprop` to see the wm class and name of an X client.
        #     # default_float_rules include: utility, notification, toolbar, splash, dialog,
        #     # file_progress, confirm, download and error.
        #     # GPG key password entry
        *layout.Floating.default_float_rules,
        Match(title="Confirmation"),  # tastyworks exit box
        Match(title="Qalculate!"),  # qalculate-gtk
        Match(wm_class="kdenlive"),  # kdenlive
        Match(wm_class="pinentry-gtk-2"),
    ]
)


# Other Configs
auto_fullscreen = True
auto_minimize = True
focus_on_window_activation = "smart"
reconfigure_screens = True


def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)


def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


# Follows a application when it's opened on a different workspace that you're on
# @hook.subscribe.client_managed
# def show_window(window):
#     window.group.cmd_toscreen()
# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
